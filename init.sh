#!/bin/bash

if [ $(id -u) -ne 0 ]; then
    echo "Are you root?"
    exit
fi

# Bind WP sources directory, implemented to avoid creation of redundant data
WP_DIR='wordpress-sources'

if [ ! -d "nginx/$WP_DIR" ]; then
    mkdir nginx/$WP_DIR
fi

if [ ! -d "php/$WP_DIR" ]; then
    mkdir php/$WP_DIR
fi

mount --bind wordpress-sources nginx/$WP_DIR
mount --bind wordpress-sources php/$WP_DIR
