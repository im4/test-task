1. Clone the repo
2. Run ./init.sh as root. Creates and mounts directories for wordpress sources.
3. docker-compose up

Notes:
127.0.0.1:8080 will lead you to readme.html
localhost:8080/index.php will lead you to WP installation page.
Since this is out of scope, the database is not created and configured properly, but it is possible to connect to the database server.
